(function($){
    $.fn.sb_form = function(method) {

        var opt = {
            'is_send_sender': true,
            'is_attach_file_with_letter': false,
            'show_form_after_sending': true,
            'hide_children_after_sending': true,
            'time_show': 5500,
            'redirect_url': null
        };

        var methods = {
            init : function(options) {
                this.each(function() {
                    function validate(fields){
                        var mes;

                        var $error_fields = $form.find('.error_field');
                        $error_fields.removeClass('error_field');
                        $form.find('.error').hide();
                        var valid = true;

                        $.each(fields, function(){
                            if ($(this).val() == '') {
                                if (/captcha_1$/.test($(this).attr('id'))) mes = sb_form_base_lang.type_chars;
                                else {
                                    if ($(this).attr('data-required')) mes = $(this).attr('data-required');
                                    else mes = sb_form_base_lang.this_field_is_required;
                                }
                                $('#err_'+$(this).attr('name')).html(mes).show();
                                $(this).addClass('error_field');
                                if (valid) $(this).focus(); // for focus on first unfilled input
                                valid = false;
                            }
                        });

                        return valid;
                    }

                    if (options) $.extend(true, opt, options);


                    var $form = $(this);
                    $form.attr('data-form_token', Math.random().toString(36));


                    var $ch = $form.parent().find('.ch');

                    var $item_row = $form.find('.item_row');
                    var $all_fields = $form.find('input, select, textarea');
                    var $required_fields = $all_fields.filter(function() {
                        return $(this).parents('.item_row').hasClass('required');
                    });


                    /* --- bind methods --- */
                    $all_fields.keydown(function(){
                        $(this).removeClass('error_field');
                    });

                    formUploader.prepareForm($form.get(0));

                    var $submit = $form.find('.submit');

                    $submit.attr({'data-initial_title': $submit.val()});

                    $submit.click(function(){
                        if (!$form.attr('data-wait')){
                            if (validate($required_fields)){
                                function clean_fields(fields){
                                    $.each(fields, function(){
                                        if ($(this).attr('data-default')) $(this).val($(this).attr('data-default'));
                                        else if ($(this)[0].tagName == 'SELECT') $(this).val($(this).find('option:first-child').val());
                                        else $(this).val('');
                                    });

                                    if ($form.attr('data-clean')){
                                        $form.attr('data-clean', 'True');
                                        $form.find('.total_info').hide();
                                        $form.find('.files').empty();
                                    }
                                }

                                var data = '&form_token='+$form.attr('data-form_token')+'&prefix='+$form.data('prefix')+'&plugin_id='+$form.attr('id').replace('sb_form_', '');

                                $.post($form.data('send_letter_url'), $form.serialize()+data)
                                    .done(function(data) {
                                        if (data.status == 'success') {
                                            function show_form_if_it_is_allowed() {
                                                if (opt.show_form_after_sending) {
                                                    setTimeout(function() {
                                                        $form.fadeIn('fast');
                                                    }, 200);
                                                }
                                            }

                                            var $default = $ch.find('.default');
                                            var $fin = $ch.find('.fin');
                                            var show_type = '';

                                            if ($default.length != 0 && $fin.length != 0) {
                                                show_type = 'show_default_and_fin'
                                            } else if ($default.length != 0) {
                                                show_type = 'show_only_default'
                                            } else if ($fin.length != 0) {
                                                show_type = 'show_only_fin'
                                            }

                                            $form.fadeOut('fast', function () {
                                                if (show_type == 'show_only_fin') $fin.show(); else $default.show();
                                            });

                                            setTimeout(function() {
                                                if (opt.redirect_url) {
                                                    document.location.href = opt.redirect_url;
                                                } else {
                                                    if (show_type == 'show_default_and_fin') {
                                                        $default.fadeOut('fast', function () {
                                                            $fin.fadeIn('fast');

                                                            setTimeout(function() {
                                                                if (opt.hide_children_after_sending) $fin.fadeOut(200);
                                                                show_form_if_it_is_allowed();
                                                            }, opt.time_show);

                                                        });
                                                    }

                                                    if (show_type == 'show_only_default') {
                                                        if (opt.hide_children_after_sending) $default.fadeOut(200);
                                                        show_form_if_it_is_allowed();
                                                    }

                                                    if (show_type == 'show_only_fin') {
                                                        if (opt.hide_children_after_sending) $fin.fadeOut(200);
                                                        show_form_if_it_is_allowed();
                                                    }
                                                }
                                            }, opt.time_show);
                                            clean_fields($all_fields);

                                            $form.attr('data-form_token', Math.random().toString(36));
                                        }
                                        else {
                                            for (var error_item in data.errors) {
                                                if (error_item == '__all__'){
                                                    var html = '';
                                                    for (var item in data.errors[error_item]) {
                                                        html += data.errors[error_item][item]
                                                    }
                                                    $form.find('.error_list').text(html).show();
                                                } else {
                                                    var error_item_selector = error_item;
                                                    if (/captcha$/.test(error_item_selector)) error_item_selector = error_item+'_1';

                                                    $item_row.find('.error').filter('[id$='+error_item_selector+']').html(data.errors[error_item][0]).show();
                                                    $all_fields.filter('[id$='+error_item_selector+']').addClass('error_field').focus();
                                                }
                                            }
                                        }
                                    })
                                    .fail(function() {
                                        $form.find('.error_list').text(sb_form_base_lang.error).show();
                                    })
                                    .always(function () {
                                        formUploader.clearFormLoading($form.get(0));

                                        var $captcha = $form.find('img.captcha');
                                        if ($captcha.length > 0) $captcha.click();
                                    });
                            } else {
                                formUploader.clearFormLoading($form.get(0));
                                return false
                            }
                        }
                    });
                });
            },
            fill_form : function(data) {
                this.each(function() {
                    var $all_fields = $(this).find('input, select, textarea');
                    for (var key in data) $all_fields.filter('[id$='+key+']').val(data[key]);
                });
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' +  method + '" is not exists for sb_form_base');
        }
    };
})(jQuery);