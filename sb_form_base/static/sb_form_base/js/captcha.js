(function($){
    $.fn.captcha = function(method) {
        var methods = {
            init : function() {
                this.each(function() {
                    var $form = $(this);

                    var $captcha_input = $form.find('.captcha_input');

                    var $err_captcha = $form.find('.error').filter('[id$=captcha]');
                    $err_captcha.attr('id', $err_captcha.attr('id')+'_1');

                    var $captcha = $captcha_input.find('img.captcha');

                    $form.find('input[name$="captcha_1"]').attr('required', 'required');

                    $captcha.click(function(){
                        $.getJSON($captcha_input.data('url'), {}, function(json) {
                            $form.find('input[name$="captcha_0"]').val(json.key);
                            $captcha.attr('src', json.image_url);
                        });

                        $form.find('input[name$="captcha_1"]').val('');

                        return false;
                    });

                    $captcha_input.find('.fa-refresh').click(function(){
                        $captcha.click();
                    });
                });
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' +  method + '" is not exists for captcha');
        }
    };
})(jQuery);