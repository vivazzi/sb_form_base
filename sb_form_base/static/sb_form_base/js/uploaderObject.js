/*
 * A file upload object to the server.
 * Transmitted parameters:
 * file       - File object (required)
 * url        - string, specifies where to load (mandatory)
 * onprogress - called when updating data about the boot process, takes one parameter: load status (in percent)
 * oncopmlete - called when the download is complete, takes two parameters:
 *              uploaded - contains true, if successful, and false if any errors occur;
 *              data - if successful, the server response is sent
 *
 *              If errors occurred during the download,
 *              the object's lastError property contains an error object containing two fields: code and text
 */
var uploaderObject = function(params) {

    if(!params.file || !params.url) {
        return false;
    }

    this.xhr = new XMLHttpRequest();
    this.reader = new FileReader();

    this.progress = 0;
    this.uploaded = false;
    this.successful = false;
    this.lastError = false;

    var self = this;

    self.reader.onload = function() {
        self.xhr.upload.addEventListener('progress', function(e) {
            if (e.lengthComputable) {
                self.progress = (e.loaded * 100) / e.total;
                if(params.onprogress instanceof Function) {
                    params.onprogress.call(self, Math.round(self.progress));
                }
            }
        }, false);

        self.xhr.upload.addEventListener('load', function(){
            self.progress = 100;
            self.uploaded = true;
        }, false);

        self.xhr.upload.addEventListener('error', function(){
            self.lastError = {
                code: 1,
                text: 'Error uploading on server'
            };
        }, false);

        self.xhr.onreadystatechange = function () {
            var callbackDefined = params.oncomplete instanceof Function;
            if (this.readyState == 4) {
                if(this.status == 200) {
                    self.successful = true;
                        if(callbackDefined) {
                            params.oncomplete.call(self, true, this.responseText);
                        }
                } else {
                    self.lastError = {
                        code: this.status,
                        text: 'HTTP response code is not OK ('+this.status+')'
                    };
                    if(callbackDefined) {
                        params.oncomplete.call(self, false);
                    }
                }
            }
        };

        self.xhr.open('POST', params.url);

        var formData = new FormData();
        formData.append('file', params.file);
        formData.append('form_token', params.form_token);

        self.xhr.setRequestHeader('Cache-Control', 'no-cache');
        self.xhr.setRequestHeader('X-CSRFToken', params.csrf_token);

        self.xhr.send(formData);
    };

    self.reader.readAsBinaryString(params.file);
};