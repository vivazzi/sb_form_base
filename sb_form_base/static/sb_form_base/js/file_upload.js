(function($){
    $.fn.file_upload = function(method) {
        var methods = {
            init : function() {
                this.each(function() {
                    var files_max_count = 100;
                    var files_max_size = 1024 * 1024 * 100;  // 100 МБ
                    var loaded_files = 0;
                    var files_count = 0;
                    var files_size = 0;


                    var $form = $(this);

                    var $info_c = $form.find('.info_c');
                    var $files = $info_c.find('.files');

                    var $all_fields = $form.find('input, select, textarea');
                    var $error_mes = $info_c.find('.error_mes');

                    function get_error_mes(current_files_count, current_files_size){
                        var addition_mes = '';

                        if (files_count == 0)
                            addition_mes = '<br/> ' + file_upload_lang.you_try_to_load + ' '+current_files_count + ' ' + file_upload_lang.files_with_total_weight + ' '+bytesToSize(current_files_size);
                        else
                            addition_mes =
                                '<br/> ' + file_upload_lang.you_try_to_load + ' '+(files_count+current_files_count)+' ' + file_upload_lang.files + ' ('+files_count+' ' + file_upload_lang.of_which_are_already_downloaded + ')' +
                                '<br/> ' + file_upload_lang.with_total_weight + ' '+bytesToSize(files_size+current_files_size)+' ('+bytesToSize(files_size)+' ' + file_upload_lang.of_which_are_already_downloaded + ')';

                        return file_upload_lang.limit_exceeded + ' '+
                            files_max_count +
                            ' ' + file_upload_lang.and_total_weight + ' '+ bytesToSize(files_max_size) + addition_mes;
                    }

                    function bytesToSize(bytes) {
                        var sizes = ['Bytes', 'KB', 'MB'];
                        if (bytes == 0) return 'n/a';
                        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                        return Math.round((bytes / Math.pow(1024, i))*10)/10 + ' ' + sizes[i];
                    }

                    // Вывод инфы о выбранных файлах
                    var $total_info = $info_c.find('.total_info');
                    var $files_count = $total_info.find('.count');
                    var $files_size = $total_info.find('.size');
                    function update_info() {
                        if (files_count > 0){
                            $total_info.show();
                            $files_count.text(file_upload_lang.files_selected + ': '+files_count);
                            $files_size.text(file_upload_lang.total_size_of_files + ': '+bytesToSize(files_size));
                        } else{
                            $total_info.hide();
                        }
                    }

                    // Обновление progress bar'а
                    function update_progress(bar, value, size) {
                        if (value == 100){
                            bar.parents('.progress').css({width: 'auto', float: 'left', 'margin-right': '4px'}).after('<i class="fa fa-check" aria-hidden="true"></i>');
                            bar.parent().css({border: 'none'}).text(bytesToSize(size));
                        } else
                            bar.css({width: value+'%'}).parent().css({border: '1px solid #888888'}).find('.title').text(value+'%');
                    }

                    // Отображение выбраных файлов и создание миниатюр
                    function displayFiles(files) {
                        if ($form.attr('data-clean') == 'True'){
                            loaded_files = 0;
                            files_count = 0;
                            files_size = 0;

                            $form.attr('data-clean', 'False');
                        }
                        $form.attr('data-wait', 'True');
                        var imageType = /image.*/;

                        var file_html = '';
                        var file_html_begin = '', file_html_middle = '', file_html_end = '';

                        var current_upload_files_count = 0;
                        var current_upload_files_size = 0;

                        $.each(files, function(i, file) {
                            current_upload_files_count++;
                            current_upload_files_size += file.size;
                        });

                        if (files_count + current_upload_files_count < files_max_count && files_size + current_upload_files_size < files_max_size){
                            $.each(files, function(i, file) {
                                var is_image = file.type.match(imageType);

                                file_html_begin = '<div class="file">';

                                file_html_end =
                                    '   <div class="info">' +
                                    '      <div class="info_wr">'+
                                    '         <div class="name">'+file.name+'</div>'+
                                    '         <i class="fa fa-times-circle" aria-hidden="true" title="Не прикреплять"></i>'+
                                    '      </div>'+
                                    '      <div class="progress" rel="0">' +
                                    '         <div class="title" rel="0">' + file_upload_lang.waiting + '</div>' +
                                    '         <div class="value"></div>' +
                                    '      </div>' +
                                    '      '+
                                    '   </div><div class="clearfix"></div>'+
                                    '</div>';

                                if (is_image){
                                    file_html_middle =
                                        '   <a class="fb th_wr" rel="gallery_1">'+
                                        '         <div class="th"></div>'+
                                        '   </a>';
                                } else {
                                    file_html_middle =
                                        '      <div class="f_icon ext_'+get_ext(file.name)+'"></div>';
                                }
                                file_html = file_html_begin + file_html_middle + file_html_end;
                                var $file = $(file_html).appendTo($files);

                                if (is_image){
                                    // Create FileReader object and after file is read, display thumbnail and update information about all files
                                    var img = $file.find('.th');
                                    var reader = new FileReader();
                                    reader.onload = (function(aImg) {
                                        return function(e) {
                                            aImg.css({'background-image': 'url("'+e.target.result+'")'});
                                            aImg.parents('.th_wr').attr('href', e.target.result).fancybox();
                                        };
                                    })(img);

                                    reader.readAsDataURL(file);
                                }

                                $file.get(0).file = file;

                                $file.attr({'data-size': file.size});
                                var $clear = $file.find('.fa-times-circle');

                                files_count++;
                                files_size += file.size;
                                update_info();

                                var uploadItem = $file[0];
                                var pBar = $(uploadItem).find('.progress .title');

                                formUploader.setFormFileLoading($form.get(0));

                                var upload_object = new uploaderObject({
                                    csrf_token: $form.data('csrf_token'),
                                    form_token: $form.data('form_token'),
                                    file: uploadItem.file,
                                    url: $form.data('save_file_url'),

                                    onprogress: function(percents) {
                                        update_progress(pBar, percents, uploadItem.file.size);
                                    },

                                    oncomplete: function(done, data) {
                                        data = parseJSON(data);
                                        if (data.status == 'success') {
                                            update_progress(pBar, 100, uploadItem.file.size);
                                            loaded_files++;
                                            $clear.attr('data-file-id', data.file_id);
                                            formUploader.clearFormLoading($form.get(0));
                                        }
                                        else {
                                            files_count--;
                                            $clear.click();
                                            alert(data.message);
                                        }
                                        if (loaded_files == files_count) $form.get(0).removeAttribute('data-wait');
                                    }
                                });

                                $clear.click(function(event){
                                    files_size -= $(this).parents('.file').attr('data-size');
                                    files_count--;

                                    var file_id = $(this).attr('data-file-id');

                                    if (file_id){
                                        $.post($form.data('clear_file_url'), { file_id: file_id}).done(function() {
                                            loaded_files--;
                                        });
                                    } else {
                                        upload_object.xhr.abort();
                                    }

                                    update_info();

                                    $file.remove();

                                    event = event || window.event;
                                    if (event.stopPropagation) event.stopPropagation();
                                    else event.cancelBubble = true;

                                    return false;
                                });
                            });

                        } else {
                            $error_mes.html(get_error_mes(current_upload_files_count, current_upload_files_size)).show();
                            setInterval(function(){
                                $error_mes.stop().fadeOut(4500);
                            }, 10000)
                        }
                    }

                    // Handling the file selection event via the standard input
                    var $id_file = $all_fields.filter('[type=file]');
                    $id_file.bind({
                        change: function() {
                            displayFiles(this.files);
                        }
                    });

                    // Handling the file selection event "drag and drop" when dragging files to an $drop_box item
                    var $drop_box = $form.find('.drop_box');
                    $drop_box.bind({
                        dragenter: function() {
                            $drop_box.stop().animate({'background-color': 'ECECEC'}, 150).find('span').text(file_upload_lang.release_mouse_btn);
                            return false;
                        },
                        dragleave: function() {
                            $drop_box.stop().animate({'background-color': 'fff'}, 150).find('span').html(file_upload_lang.add_file);
                            return false;
                        },
                        drop: function(e) {
                            e.preventDefault();
                            var dt = e.originalEvent.dataTransfer;
                            displayFiles(dt.files);
                            $drop_box.stop().animate({'background-color': 'fff'}, 150).find('span').html(file_upload_lang.add_file);
                            return false;
                        }
                    });
                });
            }
        };

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method "' +  method + '" is not exists for file_upload');
        }
    };
})(jQuery);