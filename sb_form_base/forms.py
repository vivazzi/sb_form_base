from django import forms

from sb_form_base import settings
from sb_form_base.base_form import FormBase
from sb_form_base.form_pool import form_pool
from sb_form_base.models import SBFormBase
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _, pgettext


class SBFormBaseForm(forms.ModelForm):
    form = forms.ChoiceField(label=_('Template'), choices=())

    class Meta:
        model = SBFormBase
        exclude = ()

    def __init__(self, *args, **kwargs):
        super(SBFormBaseForm, self).__init__(*args, **kwargs)
        self.fields['form'].choices = [('', '------'), ] + [(k, v.title) for k, v in form_pool.forms.items()]


# --- default forms ---
class FeedBackFormSimple(FormBase):
    title = _('Simple feedback')

    name = forms.CharField(max_length=255, label=pgettext('human', 'Name'), widget=forms.TextInput())
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput())
    comment = forms.CharField(label=_('Message'), widget=forms.Textarea(attrs={'rows': 6}))


class FeedBackForm(FormBase):
    title = _('Feedback')
    field_rules = (('phone', 'email'), )

    name = forms.CharField(max_length=255, label=pgettext('human', 'Name'), widget=forms.TextInput())

    phone = forms.CharField(max_length=100, label=_('Phone'), required=False, widget=forms.TextInput())
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput())

    comment = forms.CharField(label=_('Message'), widget=forms.Textarea(attrs={'rows': 6}))


class FeedBackFormMax(FormBase):
    title = _('Feedback with captcha and file attachments')
    field_rules = (('phone', 'email'), ('comment', 'file'))

    name = forms.CharField(max_length=255, label=pgettext('human', 'Name'), widget=forms.TextInput())

    phone = forms.CharField(max_length=100, label=_('Phone'), required=False, widget=forms.TextInput())
    email = forms.EmailField(label='Email', required=False, widget=forms.TextInput())

    comment = forms.CharField(label=_('Message'), required=False, widget=forms.Textarea(attrs={'rows': 6}))
    file = forms.FileField(label=_('Attachment'), required=False,
                           widget=forms.FileInput(attrs={'onchange': 'fileSelected();', 'multiple': 'true'}))


class CallbackForm(FormBase):
    title = _('Callback')

    name = forms.CharField(max_length=255, label=pgettext('human', 'Name'), widget=forms.TextInput())
    phone = forms.CharField(max_length=100, label=_('Phone'), widget=forms.TextInput())


for form_str in settings.SBL_FORMS:
    form_pool.register_form(import_string(form_str))
