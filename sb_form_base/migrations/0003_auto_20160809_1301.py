from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_form_base', '0002_sbformbase_alignment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sender',
            name='email',
            field=models.EmailField(default='', max_length=100, verbose_name='Email', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sender',
            name='phone',
            field=models.CharField(default='', max_length=100, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='senderform',
            name='title',
            field=models.CharField(default='', max_length=100, verbose_name='\u0424\u043e\u0440\u043c\u0430', blank=True),
            preserve_default=False,
        ),
    ]
