# Generated by Django 1.10.7 on 2018-08-07 03:11

from django.db import migrations, models
import djangocms_text_ckeditor.fields
import sb_core.fields
import sb_core.folder_mechanism


class Migration(migrations.Migration):

    dependencies = [
        ('sb_form_base', '0012_auto_20170820_1826'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='letterformfile',
            options={'verbose_name': 'File from feedback form', 'verbose_name_plural': 'Files from feedback form'},
        ),
        migrations.AlterModelOptions(
            name='sbformbase',
            options={'verbose_name': 'Form', 'verbose_name_plural': 'Forms'},
        ),
        migrations.AlterField(
            model_name='letterformfile',
            name='addition_data',
            field=models.DateField(auto_now_add=True, verbose_name='Date added'),
        ),
        migrations.AlterField(
            model_name='letterformfile',
            name='file',
            field=sb_core.fields.FileField(max_length=255, upload_to=sb_core.folder_mechanism.upload_to_handler, verbose_name='File'),
        ),
        migrations.AlterField(
            model_name='letterformfile',
            name='form_token',
            field=models.CharField(max_length=255, verbose_name='Form token'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='email_mes',
            field=djangocms_text_ckeditor.fields.HTMLField(default='Thank you for sending the message! The reply to your letter will be sent within 24 hours.', help_text="A message contained in a copy of the sender's letter.", verbose_name='Email-message'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='form',
            field=models.CharField(max_length=255, verbose_name='Form'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='headers_from',
            field=models.CharField(blank=True, help_text='Possible default value: Site [project name in genitive case]', max_length=150, verbose_name='From whom'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='headers_to',
            field=models.CharField(blank=True, help_text='Possible default value: Site visitor', max_length=150, verbose_name='To'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='hide_children_after_sending',
            field=models.BooleanField(default=True, verbose_name='Hide child elements after some time after using the form'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='is_attach_file_with_letter',
            field=models.BooleanField(default=False, verbose_name='Attach the contents of the message to a file?'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='is_send_sender',
            field=models.BooleanField(default=True, verbose_name='Send a copy of the letter to the sender?'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='is_use_captcha',
            field=models.BooleanField(default=False, help_text='Means of protection against spam robots.', verbose_name='Use CAPTCHA?'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='show_form_after_sending',
            field=models.BooleanField(default=True, verbose_name='Show form after use'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='subject',
            field=models.CharField(blank=True, help_text='Possible default value: Thank you for sending the message to [project name in accusative case]', max_length=150, verbose_name='Subject of letter'),
        ),
        migrations.AlterField(
            model_name='sbformbase',
            name='use_email_mes_for_template',
            field=models.BooleanField(default=True, verbose_name='Use the message after sending the form is the same as the email message'),
        ),
    ]
