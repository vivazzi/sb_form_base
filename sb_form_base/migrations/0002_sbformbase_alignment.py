from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_form_base', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbformbase',
            name='alignment',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='\u0412\u044b\u0440\u0430\u0432\u043d\u0438\u0432\u0430\u043d\u0438\u0435 \u0444\u043e\u0440\u043c\u044b', choices=[('left', '\u0432\u043b\u0435\u0432\u043e'), ('center', '\u043f\u043e \u0446\u0435\u043d\u0442\u0440\u0443'), ('right', '\u0432\u043f\u0440\u0430\u0432\u043e')]),
        ),
    ]
