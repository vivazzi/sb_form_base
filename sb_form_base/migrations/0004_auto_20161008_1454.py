from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_form_base', '0003_auto_20160809_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbformbase',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_form_base_sbformbase', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
