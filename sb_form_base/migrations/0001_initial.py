from django.db import models, migrations
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20140926_2347'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormTokenBind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('form_token', models.CharField(max_length=255, verbose_name='\u0422\u043e\u043a\u0435\u043d \u0444\u043e\u0440\u043c\u044b')),
                ('secret_token', models.CharField(max_length=255, verbose_name='\u0421\u0435\u043a\u0440\u0435\u0442\u043d\u044b\u0439 \u0442\u043e\u043a\u0435\u043d')),
            ],
            options={
                'db_table': 'sb_form_token_bind',
                'verbose_name': '\u0421\u0432\u044f\u0437\u043a\u0430 \u0442\u043e\u043a\u0435\u043d\u0430 \u0438 \u0441\u0435\u043a\u0440\u0435\u0442\u043d\u043e\u0433\u043e \u0442\u043e\u043a\u0435\u043d\u0430 \u0444\u043e\u0440\u043c\u044b',
                'verbose_name_plural': '\u0421\u0432\u044f\u0437\u043a\u0438 \u0442\u043e\u043a\u0435\u043d\u0430 \u0438 \u0441\u0435\u043a\u0440\u0435\u0442\u043d\u043e\u0433\u043e \u0442\u043e\u043a\u0435\u043d\u0430 \u0444\u043e\u0440\u043c\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LetterFormFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('file', models.FileField(upload_to='form_files', max_length=255, verbose_name='\u0424\u0430\u0439\u043b')),
                ('addition_data', models.DateField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('form_token', models.CharField(max_length=255, verbose_name='\u0422\u043e\u043a\u0435\u043d \u0444\u043e\u0440\u043c\u044b')),
            ],
            options={
                'db_table': 'sb_form_letter_from_file',
                'verbose_name': '\u0424\u0430\u0439\u043b \u043e\u0442 \u0444\u043e\u0440\u043c\u044b \u043e\u0431\u0440\u0430\u0442\u043d\u043e\u0439 \u0441\u0432\u044f\u0437\u0438',
                'verbose_name_plural': '\u0424\u0430\u0439\u043b\u044b \u043e\u0442 \u0444\u043e\u0440\u043c\u044b \u043e\u0431\u0440\u0430\u0442\u043d\u043e\u0439 \u0441\u0432\u044f\u0437\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SBFormBase',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('form', models.CharField(max_length=255, verbose_name='\u0424\u043e\u0440\u043c\u0430')),
                ('is_send_sender', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u044f\u0442\u044c \u043a\u043e\u043f\u0438\u044e \u043f\u0438\u0441\u044c\u043c\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044e?')),
                ('is_attach_file_with_letter', models.BooleanField(default=False, verbose_name='\u041f\u0440\u0438\u043a\u0440\u0435\u043f\u043b\u044f\u0442\u044c \u0441\u043e\u0434\u0435\u0440\u0436\u0438\u043c\u043e\u0435 \u043f\u0438\u0441\u044c\u043c\u0430 \u0432 \u0444\u0430\u0439\u043b?')),
                ('result', djangocms_text_ckeditor.fields.HTMLField(default='\u0421\u043f\u0430\u0441\u0438\u0431\u043e \u0437\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0443 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f! \u041e\u0442\u0432\u0435\u0442 \u043d\u0430 \u0432\u0430\u0448\u0435 \u043f\u0438\u0441\u044c\u043c\u043e \u0431\u0443\u0434\u0435\u0442 \u043f\u0440\u0438\u0441\u043b\u0430\u043d \u0432 \u0442\u0435\u0447\u0435\u043d\u0438\u0435 24 \u0447\u0430\u0441\u043e\u0432.', verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043f\u043e\u0441\u043b\u0435 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438 \u0444\u043e\u0440\u043c\u044b')),
            ],
            options={
                'db_table': 'sb_form_base',
                'verbose_name': '\u0424\u043e\u0440\u043c\u0430',
                'verbose_name_plural': '\u0424\u043e\u0440\u043c\u044b',
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Sender',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('phone', models.CharField(max_length=100, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('email', models.EmailField(max_length=100, null=True, verbose_name='Email', blank=True)),
                ('is_send_yourself', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u044f\u0442\u044c \u043f\u0438\u0441\u044c\u043c\u0430 \u0441\u0435\u0431\u0435?')),
                ('is_send_us', models.BooleanField(default=True, verbose_name='\u041c\u043e\u0436\u043d\u043e \u043b\u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u044f\u0442\u044c \u043f\u0438\u0441\u044c\u043c\u0430 \u043d\u0430\u043c (\u0432 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044e)?')),
                ('reg_date', models.DateField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438')),
                ('token', models.CharField(max_length=255, verbose_name='\u0422\u043e\u043a\u0435\u043d')),
            ],
            options={
                'db_table': 'sb_form_sender',
                'verbose_name': '\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c \u043f\u0438\u0441\u0435\u043c \u0432 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044e',
                'verbose_name_plural': '\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u0438 \u043f\u0438\u0441\u0435\u043c \u0432 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SenderForm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, null=True, verbose_name='\u0424\u043e\u0440\u043c\u0430', blank=True)),
                ('sender', models.ForeignKey(related_name='forms', verbose_name='\u041e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044c', to='sb_form_base.Sender', on_delete=models.CASCADE)),
            ],
            options={
                'db_table': 'sb_form_sender_form',
                'verbose_name': '\u0424\u043e\u0440\u043c\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044f',
                'verbose_name_plural': '\u0424\u043e\u0440\u043c\u044b \u043e\u0442\u043f\u0440\u0430\u0432\u0438\u0442\u0435\u043b\u044f',
            },
            bases=(models.Model,),
        ),
    ]
