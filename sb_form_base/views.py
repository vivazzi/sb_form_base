import json
from os.path import basename

from django.core.mail import mail_admins
from django.http import HttpResponse
from django.conf import settings
from django.utils.translation import gettext as _

from sb_core.utils.sb_utils import transliterate, send_mails_if_debug
from sb_core.utils.os_utils import get_free_space
from sb_form_base.form_pool import form_pool
from sb_form_base.models import LetterFormFile, SBFormBase


try:
    from post_office import mail
except ImportError:
    mail = None

MIN_FREE_SIZE = 1024 * 1024 * 1024  # GB


def save_file(request):
    if 'file' in request.FILES:
        filename = request.FILES['file'].name
        filename = transliterate(filename[filename.rfind('/')+1:])

        f = request.FILES['file']
        f.name = filename

        new_file = LetterFormFile(file=f, form_token=request.POST['form_token'])
        new_file.save()

        res = {'status': 'success', 'file_id': new_file.id}

        if get_free_space() < MIN_FREE_SIZE:
            mail_admins(_('[{}] Warning! Few server space').format(settings.SHORT_COMPANY_NAME),
                        _('The server left less than 1 GB.'))

    else:
        res = {'status': 'error', 'message': _('Information about file is not found')}

    return HttpResponse(json.dumps(res), content_type='application/json')


def clear_file(request):
    res = False

    if 'file_id' in request.POST:
        file_id = request.POST['file_id']
        LetterFormFile.objects.get(id=file_id).delete()
        res = True

    return HttpResponse(json.dumps(res), content_type='application/json')


def send_form(request):
    res = {'status': 'success', 'message': '', 'errors': ''}

    if 'plugin_id' in request.POST:
        plugin = SBFormBase.objects.filter(id=request.POST['plugin_id'])
        if plugin:
            plugin = plugin[0]

            form = form_pool.forms[plugin.form](request.user, request.POST, prefix=request.POST.get('prefix', ''))

            if plugin.is_use_captcha and request.user.is_anonymous:
                try:
                    from captcha.fields import CaptchaField
                    form.fields['captcha'] = CaptchaField(label=_('Symbols is on picture'))
                except ImportError:
                    pass

            if form.is_valid():
                form_token = request.POST.get('form_token')
                files_from_bd = LetterFormFile.objects.filter(form_token=form_token)
                file_attachments = {basename(file_item.file.path): file_item.file.path for file_item in files_from_bd}

                form_data = form.cleaned_data
                user_name = form_data.get('name')
                user_email = form_data.get('email')

                has_file_field = bool(files_from_bd)

                form.send_email_to_managers(request, user_email=user_email, plugin=plugin, has_file_field=has_file_field,
                                            file_attachments=file_attachments, files_from_bd=files_from_bd,
                                            user_name=user_name)
                form.send_email_to_sender(request, user_email=user_email, plugin=plugin, has_file_field=has_file_field,
                                          file_attachments=file_attachments, files_from_bd=files_from_bd)

                # delete form files
                files_from_bd.delete()

                send_mails_if_debug()

                form.post_handler(request)
            else:
                res.update({'status': 'error',
                            'message': _('Please, check the data entry and correct the errors.'),
                            'errors': form.errors})
        else:
            res = {'status': 'error', 'message': '', 'errors': _('plugin_id is not found')}

    return HttpResponse(json.dumps(res), content_type='application/json')
