from datetime import datetime, timedelta

from django.core.management.base import BaseCommand

from sb_form_base.models import LetterFormFile


class Command(BaseCommand):
    """
        Remove old attachment files, which have not been removed by what ever the reason
    """
    args = ''
    help = 'Clear old letter_form_files.'

    def handle(self, *args, **options):
        file_life = 3
        LetterFormFile.objects.filter(addition_data__lte=datetime.now().date() - timedelta(days=file_life)).delete()
