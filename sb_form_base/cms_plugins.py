from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.constants import BASE
from sb_core.templatetags.sb_tags import has_file_field
from sb_form_base.form_pool import form_pool
from sb_form_base.forms import SBFormBaseForm
from sb_form_base.models import SBFormBase
from sb_core.utils.sb_utils import get_random_seq
from django.utils.translation import gettext as _


class SBContactFormPlugin(CMSPluginBase):
    module = BASE
    model = SBFormBase
    name = _('Form')
    render_template = 'sb_form_base/sb_form_base.html'
    form = SBFormBaseForm
    allow_children = True

    fieldsets = (
        ('', {
            'fields': ('form', 'is_send_sender', 'is_attach_file_with_letter', 'is_use_captcha')
        }),
        (_('Actions after usage of the form'), {
            'fields': ('email_mes', 'use_email_mes_for_template', 'show_form_after_sending', 'hide_children_after_sending')
        }),
        (_('Additional'), {
            'fields': ('subject', ('headers_from', 'headers_to'))
        })
    )

    def render(self, context, instance, placeholder):
        context = super(SBContactFormPlugin, self).render(context, instance, placeholder)

        form = form_pool.forms.get(instance.form)
        if form:
            self.render_template = form.template
            form = getattr(form, 'form', form)(prefix=get_random_seq(10), user=context['request'].user)

            if instance.is_use_captcha and context['request'].user.is_anonymous:
                try:
                    from captcha.fields import CaptchaField
                    form.fields['captcha'] = CaptchaField(label=_('Symbols is on picture'))
                except ImportError:
                    pass

            context['has_file_field'] = has_file_field(form)

        context['form'] = form

        if form:
            context.update(form.extend_context(context))
        return context

plugin_pool.register_plugin(SBContactFormPlugin)
