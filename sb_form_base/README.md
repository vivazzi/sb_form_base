# Troubleshoots

## Google error: 550, 5.7.1 

```
(550, b'5.7.1 This message is not RFC 5322 compliant. There are multiple To headers.
5.7.1 To reduce the amount of spam sent to Gmail, this message has been 5.7.1 blocked. 
For more information, go to
5.7.1  https://support.google.com/mail/?p=RfcMessageNonCompliant and review 5.7.1 RFC 5322 specifications. 
c19-20020a056512325300b0051360320c79sm226836lfr.86 - gsmtp')
```

For some reason, when sending a letter, two headers are formed. That's it, but Gmail prohibits the use of two headers, which is why the error occurs.

Solution: Use mail.ru provider or remove `To` header. See: `send_email_to_managers` and `send_email_to_sender` in `sb_form_base/base_form.py`