from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBFormBaseConfig(AppConfig):
    name = 'sb_form_base'
    verbose_name = _('Forms')
