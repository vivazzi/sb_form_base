from django.urls import path

from sb_form_base.views import send_form, save_file, clear_file

urlpatterns = [
    path('save_file/', save_file, name='save_file'),
    path('clear_file/', clear_file, name='clear_file'),

    path('send_letter/', send_form, name='send_letter'),
]
