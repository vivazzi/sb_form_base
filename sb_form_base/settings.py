from django.conf import settings


SBL_FORMS = getattr(settings, 'SBL_FORMS', (
    'sb_form_base.forms.FeedBackFormSimple',
    'sb_form_base.forms.FeedBackForm',
))
