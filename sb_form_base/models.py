from os.path import basename

from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import gettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField

from sb_core.fields import FileField
from sb_core.models import FileModel
from sb_form_base.form_pool import form_pool


class SBFormBase(CMSPlugin):
    form = models.CharField(_('Form'), max_length=255)

    email_mes = HTMLField(_('Email-message'),
                          default=_('Thank you for sending the message! '
                                    'The reply to your letter will be sent within 24 hours.'),
                          help_text=_("A message contained in a copy of the sender's letter."))

    use_email_mes_for_template = models.BooleanField(
        _('Use the message after sending the form is the same as the email message'), default=True
    )
    show_form_after_sending = models.BooleanField(_('Show form after use'), default=True)
    hide_children_after_sending = models.BooleanField(_('Hide child elements after some time after using the form'),
                                                      default=True)

    is_send_sender = models.BooleanField(_('Send a copy of the letter to the sender?'), default=True)
    is_attach_file_with_letter = models.BooleanField(_('Attach the contents of the message to a file?'), default=False)

    is_use_captcha = models.BooleanField(_('Use CAPTCHA?'), default=False,
                                         help_text=_('Means of protection against spam robots.'))

    # email options
    subject = models.CharField(_('Subject of letter'), max_length=150, blank=True,
                               help_text=_('Possible default value: '
                                           'Thank you for sending the message to [project name in accusative case]'))

    headers_from = models.CharField(_('From whom'), max_length=150, blank=True,
                                    help_text=_('Possible default value: Site [project name in genitive case]'))

    headers_to = models.CharField(_('To'), max_length=150, blank=True,
                                  help_text=_('Possible default value: Site visitor'))

    def __str__(self):
        form = form_pool.forms.get(self.form, None)
        if form:
            return str(form.title)
        return str(_('<empty>'))

    class Meta:
        db_table = 'sb_form_base'
        verbose_name = _('Form')
        verbose_name_plural = _('Forms')


class LetterFormFile(FileModel):
    file = FileField(_('File'))
    addition_data = models.DateField(_('Date added'), auto_now_add=True)
    form_token = models.CharField(_('Form token'), max_length=255)

    def __str__(self):
        return basename(self.file)

    class Meta:
        db_table = 'sb_form_letter_from_file'
        verbose_name = _('File from feedback form')
        verbose_name_plural = _('Files from feedback form')
