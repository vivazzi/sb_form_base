from django.core.exceptions import ImproperlyConfigured

from sb_form_base.base_form import FormBase


class FormPool(object):
    def __init__(self):
        self.forms = {}

    def register_form(self, form):
        if not issubclass(form, FormBase):
            raise ImproperlyConfigured('Form must be subclasses of FormBase, {} is not.'.format(form))

        form_name = form.__name__
        if form_name in self.forms:
            raise FormAlreadyRegistered('Cannot register {0}, a form with this name ({0}) '
                                        'is already registered.'.format(form, form_name))

        form.value = form_name
        self.forms[form_name] = form

    def unregister_form(self, form):
        form_name = form.__name__
        if form_name not in self.forms:
            raise FormNotRegistered('The form %r is not registered' % form)
        del self.forms[form_name]


class FormAlreadyRegistered(Exception):
    pass


class FormNotRegistered(Exception):
    pass


form_pool = FormPool()
