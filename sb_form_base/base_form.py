from functools import partial

from django import forms
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import force_str
from django.utils.translation import gettext as _

from sb_form_base.utils import render_to_pdf
from sb_core.utils.sb_utils import transliterate, Managers


try:
    from post_office import mail
except ImportError:
    mail = None

from sb_core.constants import DEFAULT_MANAGER_GROUP


class FormBase(forms.Form):
    title = None
    manager_group = DEFAULT_MANAGER_GROUP
    field_rules = ()

    template = 'sb_form_base/sb_form_base.html'
    manager_template = 'sb_form_base/email/for_manager.html'
    pdf_template = 'sb_form_base/email/pdf.html'
    sender_template = 'sb_form_base/email/for_sender.html'

    subject = _('Thanks for the sending message to {}').format(getattr(settings, 'COMPANY_NAME_ACCUSATIVE', 'COMPANY_NAME'))
    headers_from = _('Site {}').format(getattr(settings, 'COMPANY_NAME_GENITIVE', 'COMPANY_NAME'))
    headers_to = _('Site visitor')

    def post_handler(self, request):
        """
        You can use this method for the handling post-operations such as the adding new user to subscription and etc.
        """
        pass

    def extend_context(self, context):
        """
        You can use this method for the extending context for template.
        """
        return context

    def _get_FIELD_display(self, field, value):
        return force_str(dict(field.choices).get(value, value), strings_only=True)

    def __init__(self, user, *attrs, **kwargs):
        if not self.title:
            raise ImproperlyConfigured('Form class {} must have "title" attribute'.format(self.value))

        for name, field in self.base_fields.items():
            if hasattr(field, 'choices'):
                setattr(self, 'get_{}_display'.format(name), partial(self._get_FIELD_display, field))

        super(FormBase, self).__init__(*attrs, **kwargs)

    def clean(self):
        cleaned_data = super(FormBase, self).clean()
        for rule in self.field_rules:
            if not any([cleaned_data.get(item) for item in rule]):
                required_fields = ', '.join([self.fields[item].label for item in rule])
                raise forms.ValidationError(_('Fill in at least one of the fields: {}').format(required_fields))

        if 'name' in cleaned_data and 'http' in cleaned_data['name']:
            raise forms.ValidationError(_('Field "name" must not consist of "http"'))

        if 'name' in cleaned_data and 'comment' in cleaned_data and cleaned_data['name'] == cleaned_data['comment']:
            raise forms.ValidationError(_('Use different value for "name" and "comment" fields'))

    def send_email_to_managers(self, request, **kwargs):
        plugin = kwargs.pop('plugin')
        has_file_field = kwargs.pop('has_file_field')
        file_attachments = kwargs.pop('file_attachments')
        files_from_bd = kwargs.pop('files_from_bd')
        user_name = kwargs.pop('user_name')

        me = _('Site visitor {} <{}>').format(settings.COMPANY_NAME_GENITIVE, settings.EMAIL_HOST_USER)
        subject = '[{}] {}'.format(settings.COMPANY_NAME, _('Message from site'))

        email_data = {'form': self, 'has_file_field': has_file_field, 'request': request}
        msg = render_to_string(self.manager_template, email_data)

        managers = Managers(self.manager_group)
        for manager in managers.get_managers():
            headers = {'To': managers.get_manager_name(manager, settings.COMPANY_NAME_GENITIVE)}
            if settings.EMAIL_BACKEND == 'post_office.EmailBackend':
                attachments = file_attachments
                if plugin.is_attach_file_with_letter:
                    pdf = render_to_pdf(self.pdf_template, email_data)
                    attachments['Message_from_{}.pdf'.format(transliterate(user_name))] = ContentFile(pdf)

                mail.send(manager.email, me, subject=subject, message=msg, html_message=msg,
                          attachments=attachments, headers=headers)
            else:
                email = EmailMessage(subject, msg, me, (manager.email,), headers=headers)
                email.content_subtype = 'html'

                if plugin.is_attach_file_with_letter:
                    pdf = render_to_pdf(self.pdf_template, email_data)
                    email.attach('Message_from_{}.pdf'.format(transliterate(user_name)), pdf, 'application/pdf')

                for file_item in files_from_bd:
                    email.attach_file(file_item.file.path)

                email.send()

    def send_email_to_sender(self, request, **kwargs):
        user_email = kwargs.pop('user_email')
        plugin = kwargs.pop('plugin')
        has_file_field = kwargs.pop('has_file_field')
        file_attachments = kwargs.pop('file_attachments')
        files_from_bd = kwargs.pop('files_from_bd')

        if user_email and plugin.is_send_sender:
            headers_from = plugin.headers_from or self.headers_from
            me = '{} <{}>'.format(headers_from, settings.EMAIL_HOST_USER)
            subject = plugin.subject or self.subject
            email_data = {'form': self, 'has_file_field': has_file_field, 'mes': plugin.email_mes}
            msg = render_to_string(self.sender_template, email_data)

            headers_to = plugin.headers_to or self.headers_to
            headers = {'To': '{} <{}>'.format(headers_to, user_email)}

            if settings.EMAIL_BACKEND == 'post_office.EmailBackend':
                mail.send(user_email, me, subject=subject, message=msg, html_message=msg, attachments=file_attachments,
                          headers=headers)
            else:
                email = EmailMessage(subject, msg, me, (user_email,), headers=headers)
                email.content_subtype = 'html'

                for file_item in files_from_bd:
                    email.attach_file(file_item.file.path)

                email.send()
