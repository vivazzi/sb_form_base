=======
sb_form_base
=======

sb_form_base is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds form to page.


Installation
============

sb_form_base requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_form_base in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_form_base


Configuration 
=============

1. Add "sb_form_base" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_form_base',
        ...
    )

2. Run `python manage.py migrate` to create the sb_form_base models.  
